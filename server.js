const app = require('express')();
const http = require('http').Server(app);
const mongoose = require('mongoose');
const models = require('./models')
const { v4: uuidv4 } = require('uuid');
const io = require('socket.io')(http, {
    cors: {
        origin: '*',
    }
});

mongoose.connect('mongodb://localhost:27017/cognite', {useNewUrlParser: true, useUnifiedTopology: true, keepAlive: true});

app.get('/', function(req, res) {
    res.json({health: 'OK'});
 }); 

io.on('connection', (socket) => {

    socket.on('setChannelDraft', async (params) => {
        const {currentChannel, message} = params;
        const updateChannel = await models.Channel.updateOne({channelId: currentChannel}, {draftMessage: message})
    })

    socket.on('createChannel', (params) => {
        models.Channel({name: params.name, channelId: uuidv4()}).save()
        .then(() => {
            models.Channel.find({}, (err, channels) => {
                socket.emit('listChannels', channels)
            })
        })
    })

    socket.on('joinChannel', (params) => {
        console.log('JOIN: ', params.channel)
        socket.join(params.channel)
        models.Channel.findOne({channelId: params.channel}, (err, channel) =>{
            models.Message.find({channelId: params.channel}, (err, messages) => {
                let response = { messages }
                if(channel){
                    response = {...response, draftMessage: channel.draftMessage}
                }
                socket.emit('listMessages', response)
            })
        })
    })

    socket.on('leaveChannel', (params) => {
        console.log('LEAVE: ', params.channel)
        socket.leave(params.channel)
    })

    socket.on('sendMessage', (params) => {
        models.Message({
            avatar: params.image,
            name: params.name,
            message: params.message,
            channelId: params.channelId,
            date: Date.now()
        }).save()
        .then(() => {
            models.Message.find({channelId: params.channelId}, async (err, messages) => {
                const updateChannel = await models.Channel.updateOne({
                    channelId: params.channelId, 
                    draftMessage: params.message}, 
                    {draftMessage: ""})
                console.log(updateChannel)
                socket.emit('listMessages', { messages, draftMessage: ''})
                socket.to(params.channelId).emit('listMessages', { messages, draftMessage: ''})
            })
        })
    })

    socket.on('showChannels', (params) => {
        models.Channel.find({}, (err, channels) => {
            socket.emit('listChannels', channels)
        })
    })
})

http.listen(5500, () => console.log('Running on :5500'));
 