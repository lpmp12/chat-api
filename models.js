const mongoose = require('mongoose');

const channelSchema = new mongoose.Schema({
    channelId: String,
    name: String,
    draftMessage: String
});

const messageSchema = new mongoose.Schema({
    channelId: String,
    date: String,
    avatar: String,
    name: String,
    message: String
});

const Channel = mongoose.model('channels', channelSchema);
const Message = mongoose.model('messages', messageSchema);

module.exports = {
    Channel,
    Message
}