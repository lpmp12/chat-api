### Chat API

## Which stack I'm using here?
* Node.js
* SocketIO
* MongoDB

## Steps to run the project
* Install MongoDB and create a database, then set the DB name in the string connection at server.js:12.
* yarn install
* node server.js